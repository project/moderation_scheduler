<?php

namespace Drupal\moderation_scheduler\Plugin\views\query;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\NodeInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;

/**
 * ModerationSchedulerQuery views query plugin expose the results to views.
 *
 * @ViewsQuery(
 *   id = "moderation_scheduler_query",
 *   title = @Translation("Moderation Scheduler Query"),
 *   help = @Translation("Query against the scheduled time field.")
 * )
 */
class ModerationSchedulerQuery extends QueryPluginBase {

  protected $conditions = [];
  protected $fields = [];
  protected $allItems = [];
  protected $orderBy = [];

  /**
   * {@inheritdoc}
   */
  public function execute(ViewExecutable $view) {
    $moderation_scheduler_interface = \Drupal::service('moderation_scheduler.services');
    $user = \Drupal::currentUser();
    $db_timezone = 'UTC';
    $d_timezone = \Drupal::config('system.date')->get('timezone.default');
    $u_timezone = $user->getTimezone() ? $user->getTimezone() : $d_timezone;
    $nodes = $moderation_scheduler_interface->fieldScheduledTimeRevision(NULL, NULL, FALSE);
    $date = new DrupalDateTime();
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $date = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $language = \Drupal::service('language.default')->get();
    $default_language = $language->getId();
    $key_array = [];
    $index = 0;

    if ($nodes) {
      foreach ($nodes as $node) {
        if ($node instanceof NodeInterface) {
          // var_dump($n->getTitle());
          $languages = $node->getTranslationLanguages();
          foreach ($languages as $lang) {
            $entity = $node->hasTranslation($lang->getId()) ? $node->getTranslation($lang->getId()) : NULL;
            if ($entity->field_scheduled_time->value && !empty($entity->field_scheduled_time->date) && $entity->status->getString() !== 1 && $entity->get('moderation_state')->getString() !== 'published' && $entity->get('moderation_state')->getString() === 'draft' && !$node->isPublished() && !$entity->isPublished() && !in_array($entity->getTitle(), $key_array)) {
              $schedule = $entity->field_scheduled_time->value;
              $scheduled_date = $entity->field_scheduled_time->date;
              $scheduled_formatted_date = \Drupal::service('date.formatter')->format($scheduled_date->getTimestamp(), 'custom', 'd/m/Y - H:i');
              if ($schedule !== NULL && !empty($schedule)) {
                $state = $moderation_scheduler_interface->returnState($entity);
                $row['title'] = $entity->getTitle();
                $row['content_type'] = $entity->type->entity->label();
                $row['field_scheduled_time'] = $scheduled_formatted_date;
                $row['author'] = $entity->getOwner()->getDisplayName();
                $row['status'] = $state;
                $row['revision'] = $entity->get('vid')->value;

                // 'index' key is required.
                $row['index'] = $index++;
                $view->result[] = new ResultRow($row);
                array_push($key_array, $row['title']);
              }
            }
          }

        }

      }
    }
    return $key_array;
  }

  /**
   * Hook_ensureTable.
   */
  public function ensureTable($table, $relationship = NULL) {
    return '';
  }

  /**
   * Hoox_addField.
   */
  public function addField($table, $field, $alias = '', $params = []) {
    return $field;
  }

  /**
   *
   */
  public function addOrderBy($table, $field = NULL, $order = 'ASC', $alias = '', $params = []) {
    $this->orderBy = [
      'field' => $field,
      'order' => $order,
    ];
  }

}
