<?php

namespace Drupal\moderation_scheduler;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Defines a moderation scheduler interface.
 */
class ModerationSchedulerService {

  use StringTranslationTrait;
  /**
   * Module handler service object.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Config Factory service object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Entity Manager service object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;


  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a SchedulerManager object.
   */
  public function __construct(ModuleHandler $moduleHandler, ConfigFactory $configFactory, EntityTypeManager $entityTypeManager, Connection $database, LanguageManagerInterface $languageManager, EventDispatcherInterface $eventDispatcher) {
    $this->moduleHandler = $moduleHandler;
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entityTypeManager;
    $this->database = $database;
    $this->languageManager = $languageManager;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * Main function to publish scheduled node.
   *
   * @return array
   *   Array of published nodes.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function publishScheduled() {
    // Event dispatcher.
    $dispatcher = $this->eventDispatcher;

    $node_storage = $this->entityTypeManager->getStorage('node');
    $state = 'published';
    $default_language = $this->languageManager->getDefaultLanguage()->getId();

    // Get a date string suitable for use with entity query.
    $date = new DrupalDateTime();
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $date = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    $languages = array_keys($this->languageManager->getLanguages());
    $result = [];

    // Get all nodes with scheduled time.
    $nodes = $this->fieldScheduledTimeRevision(NULL, NULL, FALSE);

    if ($nodes) {
      foreach ($nodes as $node) {
        if ($node instanceof NodeInterface) {
          // var_dump($n->getTitle());
          $languages = $node->getTranslationLanguages();
          foreach ($languages as $lang) {

            $entity = $node->hasTranslation($lang->getId()) ? $node->getTranslation($lang->getId()) : NULL;
            if ($entity->field_scheduled_time->value) {
              $schedule = $entity->field_scheduled_time->value;
              if ($schedule !== NULL && !empty($schedule) && $schedule <= $date) {
                $this->resetFields($entity, $lang->getId());
                $event = new ModerationSchedulerEvent($entity);
                $dispatcher->dispatch($event, ModerationSchedulerEvents::PUBLISH);
                $state = $this->returnState($entity);
                $result[] = $entity->getTitle() . ' (' . $state . '), ' . $schedule;
              }
            }
          }
          $language = $this->languageManager->getLanguage($default_language);
          $this->languageManager->setConfigOverrideLanguage($language);
          $original = $node->entity;
          if ($original && $original->get('field_scheduled_time')->value) {
            $schedule = $original->get('field_scheduled_time')->value;
            // var_dump($schedule);die;
            if ($schedule !== NULL && !empty($schedule) && $schedule <= $date) {
              $this->resetFields($original, $default_language);
              $event = new ModerationSchedulerEvent($original);
              $dispatcher->dispatch($event, ModerationSchedulerEvents::PUBLISH);
              $state = $this->returnState($original);
              $result[] = $original->getTitle() . ' (' . $state . '), ' . $schedule;
            }
          }
        }
      }
    }
    else {
      return ['no_scheduled_contents' => $this->t("no scheduled content to publish")];
    }

    // Return result of scheduling.
    return $result ?? ['no_scheduled_contents' => $this->t("no scheduled content to publish")];
  }

  /**
   * Helper method to return node state.
   *
   * @param object $node
   *   Object of node.
   * @param string $lang
   *   String langcode of node.
   *
   * @return string
   *   String of node status.
   */
  public function resetFields($node, $lang) {
    $default_language = $this->languageManager->getDefaultLanguage()->getId();
    $language = $this->languageManager->getLanguage($lang);
    $this->languageManager->setConfigOverrideLanguage($language);
    if ($node) {
      if ($node->hasField('field_scheduled_time')) {
        if ($lang != $default_language) {
          $node->field_scheduled_time->value = NULL;
          $node->field_scheduled_time->langcode = $lang;
        }
        else {
          $node->set('field_scheduled_time', NULL);
          $node->field_scheduled_time->langcode = $default_language;
        }
      }
      if ($node->hasField('moderation_state')) {
        if ($lang != $default_language) {
          $node->moderation_state->value = 'published';
          $node->moderation_state->langcode = $lang;
        }
        else {
          $node->set('moderation_state', 'published');
          $node->moderation_state->langcode = $default_language;
        }
      }
      $node->setPublished(TRUE);
      // $node->pubblished = TRUE;
      $node->status = 1;
      // $node->isDefaultRevision(TRUE);
      $node->setNewRevision(TRUE);
      // $node->setRevisionLogMessage('Scheduled content published');
      // $node->isDefaultRevision(FALSE);
      $node->save();
    }
    return $node;
  }

  /**
   * Helper method to return node state.
   *
   * @param object $node
   *   Object of node.
   *
   * @return string
   *   String of node status.
   */
  public function returnState($node) {
    if ($node->hasField('moderation_state')) {
      $state = $node->get('moderation_state')->value;
      $node->set('moderation_state', 'published');
    }
    else {
      $state = $node->status == 1 ? "published" : "unpublished";
    }
    return $state;
  }

  /**
   * Helper method to load latest revision of each node.
   *
   * @param array $nids
   *   Array of node ids.
   *
   * @return array
   *   Array of loaded nodes.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function loadNodes(array $nids) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $nodes = [];

    // Load the latest revision for each node.
    foreach ($nids as $nid) {
      $node = $node_storage->load($nid);
      if ($node) {
        $nodes[] = $node;
      }
    }
    return $nodes;
  }

  /**
   * Helper method to load latest revision of each translation.
   *
   * @param int $nid
   *   Integer node id.
   * @param string $langcode
   *   String langcode of node.
   * @param bool $filterDate
   *   Boolean filterDate of query.
   *
   * @return object
   *   Object of loaded node revision or [Object node].
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function fieldScheduledTimeRevision($nid, $langcode, $filterDate = TRUE) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    if ($node_storage instanceof RevisionableStorageInterface) {
      // Get a date string suitable for use with entity query.
      $date = new DrupalDateTime();
      $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
      $date = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
      $connection = $this->database;
      if ($nid) {

        // Get revision of latest tranlsation.
        $query = $connection->query(
          "SELECT revision_id, langcode, field_scheduled_time_value, entity_id 
        FROM {node_revision__field_scheduled_time} 
        WHERE langcode = :langcode 
          AND field_scheduled_time_value IS NOT NULL 
          AND field_scheduled_time_value <= :date 
          AND entity_id = :nid",
          [
            ':langcode' => $langcode,
            ':date' => $date,
            ':nid' => $nid,
          ]
        );

        $revisions = $query->fetchAll();

        if ($revisions) {
          $revision = end($revisions);

          // Get revision id from the field.
          $rev_id = $revision->revision_id;
          $schedule = $revision->field_scheduled_time_value;
          $lang = $revision->langcode;
          if ($lang == $langcode && $schedule <= $date) {

            // Load revision by lang and translation.
            $rev = $node_storage->loadRevision($rev_id);
            if ($rev) {
              $rev = $rev->getTranslation($langcode);
              $rev->set('langcode', $langcode);
              return $rev;
            }
          }
        }
      }
      elseif ($langcode) {

        // If no nid get latest tranlsations.
        $query = $connection->query(
          "SELECT revision_id, langcode, field_scheduled_time_value, entity_id 
        FROM {node_revision__field_scheduled_time} 
        WHERE langcode = :langcode 
          AND field_scheduled_time_value IS NOT NULL 
          AND field_scheduled_time_value <= :date
        ORDER BY field_scheduled_time_value DESC
        LIMIT 1",
          [
            ':langcode' => $langcode,
            ':date' => $date,
          ]
              );

        $revisions = $query->fetchAll();
        if ($revisions) {
          $nodes = [];
          foreach ($this->arrayMultidimUnique($revisions, 'entity_id') as $revision) {
            // Get revision id from the field.
            $revision_id = $revision->revision_id;
            $schedule = $revision->field_scheduled_time_value;
            $lang = $revision->langcode;
            if ($lang == $langcode && $schedule <= $date) {

              // Load revision with lang and translation.
              $rev = $node_storage->loadRevision($revision_id);
              if ($rev) {
                $rev = $rev->getTranslation($langcode);
                $rev->set('langcode', $langcode);
                array_push($nodes, $rev);
              }
            }
          }
          return count($nodes) >= 1 ? $nodes : NULL;
        }
      }
      elseif (!$langcode && !$nid) {
        // If no nid get latest tranlsations.
        // https://www.drupal.org/project/moderation_scheduler/issues/3081710
        // https://www.drupal.org/project/moderation_scheduler/issues/3080452
        // remove filter to fix view.
        if ($filterDate == TRUE) {
          $query = $connection->query(
            "SELECT revision_id, langcode, field_scheduled_time_value, entity_id 
          FROM {node_revision__field_scheduled_time} 
          WHERE field_scheduled_time_value IS NOT NULL 
            AND field_scheduled_time_value <= :date
          ORDER BY field_scheduled_time_value DESC
          LIMIT 1",
            [
              ':date' => $date,
            ]
          );
        }
        else {
          $query = $connection->query(
            "SELECT revision_id, langcode, field_scheduled_time_value, entity_id 
          FROM {node_revision__field_scheduled_time}
          WHERE field_scheduled_time_value IS NOT NULL"
                  );
        }

        $revisions = $query->fetchAll();

        if ($revisions) {

          $nodes = [];
          foreach ($this->arrayMultidimUnique($revisions, 'revision_id') as $revision) {
            // Get revision id from the field.
            $revision_id = $revision->revision_id;
            $schedule = $revision->field_scheduled_time_value;
            $lang = $revision->langcode;

            // Load revision with lang and translation.
            $rev = $node_storage->loadRevision($revision_id);
            if ($rev) {
              $rev = $rev->getTranslation($lang);
              if ($filterDate == TRUE) {
                if ($lang && $schedule <= $date) {
                  array_push($nodes, $rev);
                }
              }
              else {
                array_push($nodes, $rev);
              }
            }
          }
          return count($nodes) >= 1 ? $nodes : NULL;
        }
      }
    }
  }

  /**
   * Helper method to load arrayMultidimUnique by key.
   *
   * @param array $array
   *   Array of nodes.
   * @param string $key
   *   String of key index.
   *
   * @return array
   *   Array of [object node].   *
   */
  public function arrayMultidimUnique(array $array, $key) {
    $temp_array = [];
    $i = 0;
    $key_array = [];

    foreach ($array as $val) {
      if (!in_array($val->$key, $key_array)) {
        $key_array[$i] = $val->$key;
        $temp_array[$i] = $val;
      }
      $i++;
    }
    return $temp_array;
  }

  /**
   * Helper method to load latest revision of each translation.
   *
   * @param string $field
   *   String of field_name.
   *
   * @return bolean
   *   Bolean of result.
   */
  public function cleanFieldScheduledTimeRevision($field) {
    $db = $this->database;

    $date = new DrupalDateTime();
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $date = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    $query_revision = $db->delete('node_revision__field_scheduled_time')
      ->condition('field_scheduled_time_value', $date, '<=');
    $result_revision = $query_revision->execute();

    $query_field = $db->delete('node__field_scheduled_time')
      ->condition('field_scheduled_time_value', $date, '<=');
    $result_field = $query_field->execute();

    return ($result_revision > 0 || $result_field > 0);
  }

}
